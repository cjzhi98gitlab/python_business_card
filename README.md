# PyCon MY Camp 2022

Welcome to PyCon MY Camp 2022. This is the main repo for ACME Business Card application.
Here what are we trying to build is an application that stores the virtual business on
the people that you have met in the conference/event.

## Important Gadgets/Infrastructure:

### Slack
Please join The following [Slack Group](https://join.slack.com/t/slack-wid4508/shared_invite/zt-1gcyza9xt-TU0F~UQSL3kHKRTzHnxzfA)

### GitLab
GitLab Repo: [python_business_card](https://gitlab.com/tangingw/python_business_card/)
Please Ask for Access

### OpenShift
* Host: https://139.162.29.125:8443/console
* Username: developer
* Password: pyconmy12345

### MySQL Access
* Host: pyconmydb.pycon.my
* Username: pyconmy
* Password: pyconmy12345
