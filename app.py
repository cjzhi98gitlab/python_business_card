from flask import Flask, render_template

app = Flask(__name__)


@app.route("/<name>")
def get_index_page(name):
    if name:
        return f"Hello {name}"

    return "Hello World"


if __name__ == "__main__":
    app.run(host="0.0.0.0", debug=True)