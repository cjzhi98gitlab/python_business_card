# Reference

## Flask Reference
These are probably the reference for Flask that we might need during the conference

### Official Documentation
* [Flask Official Documentation](https://flask.palletsprojects.com/en/2.2.x/)
* [Flask Blueprint](https://flask.palletsprojects.com/en/2.2.x/blueprints/)
* [Jinja2 Templating](https://jinja.palletsprojects.com/en/3.1.x/)

## Pytest Documentation

### Official Documentaiton
* [pytest](https://docs.pytest.org/en/7.1.x/)

## RealPython
* [HTML and CSS for Python Developers](https://realpython.com/html-css-python/)
* [Python Web Applications: Deploy Your Script as a Flask App](https://realpython.com/python-web-applications/)
* [Use a Flask Blueprint to Architect Your Applications](https://realpython.com/flask-blueprint/)
* [Create a Flask Application With Google Login](https://realpython.com/flask-google-login/)
* [Token-Based Authentication With Flask](https://realpython.com/token-based-authentication-with-flask/)
